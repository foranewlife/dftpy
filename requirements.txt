numpy>=1.11.0
scipy>=0.18.0
ase>=3.22.0
importlib-metadata>=0.12;python_version<"3.8"
